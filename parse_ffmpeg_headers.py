# This file is part of Nametag.
# Copyright 2020 Austin Allman and Adam Higerd
#
# Nametag is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this software; if not, see
# <https://www.gnu.org/licenses/>.

import os, sys, subprocess

#
# Ensure dependencies are installed and have appropriate versions
#
PKGCONFIG_PACKAGE_VERSIONS = {
    'libavutil': '55',
    'libavformat': '57',
    'taglib': '1.10',
}

def check_pkgconfig_version(package, version):
    try:
        subprocess.check_call(['pkg-config', '--atleast-version=%s' % version, package])
    except subprocess.CalledProcessError:
        sys.stderr.write("The development files for '%s' could not be found or are older than version %s.\n\nPlease ensure that the development package for '%s' is installed.\n" % (package, version, package))
        sys.exit(1)

for package, version in PKGCONFIG_PACKAGE_VERSIONS.items():
    check_pkgconfig_version(package, version)

#
# Collect the include paths needed for ffmpeg
#
try:
    # TODO: Windows build paths
    include_paths = list(map(
        lambda flag: flag[2:],
        filter(
            lambda flag: flag.startswith('-I'),
            subprocess.check_output(['pkg-config', '--cflags-only-I', 'libavformat', 'libavutil']).decode().strip().split(' '),
        ),
    ))
except:
    include_paths = []

include_paths.append('/usr/include')

def find_header(name):
    for path in include_paths:
        fullpath = os.path.join(path, name)
        if os.access(fullpath, os.R_OK):
            return fullpath
    raise FileNotFoundError("Could not find '%s'" % name)

#
# Parse the header files to construct ctypes definitions
#
def get_ctype(mdef):
    if '*' in mdef and ('uint8_t' in mdef or 'char' in mdef):
        # it's a string
        return 'c_char_p'
    elif '*' in mdef:
        # it's a pointer to something besides a string
        return 'c_void_p'
    elif 'enum' in mdef or 'int' in mdef:
        if 'unsigned' in mdef:
            return 'c_uint'
        return 'c_int'
    elif 'char' in mdef:
        return 'c_char'
    elif 'int64_t' in mdef:
        return 'c_longlong'
    elif 'AVIOInterruptCB' in mdef:
        return 'AVIOInterruptCB'
    elif 'av_format_control_message' in mdef:
        # unused callback typedef
        return 'c_void_p'
    else:
        raise Exception('could not determine ctype for %s' % repr(mdef))

AVFormatContext = []
metadata_index = None
libavformat_version = None

with open(find_header('libavformat/version.h'), 'r') as f:
    for line in f:
        if '#define' in line:
            define = line.strip().split(' ')
            if define[1] == 'LIBAVFORMAT_VERSION_MAJOR':
                libavformat_version = int(define[-1])
                break

with open(find_header('libavformat/avformat.h'), 'r') as f:
    found_struct = False
    comment = False
    concat_line = None
    for line in f:
        if concat_line:
            line = concat_line + line
            concat_line = None
        line = line.strip()
        if '/*' in line:
            comment = True
            comment_start = line.index('/*')
            line = line[:comment_start]
        if comment:
            if '*/' in line:
                comment = False
                comment_end = line.index('*/')
                line = line[comment_end + 2:]
            else:
                continue
        if not found_struct:
            if 'AVFormatContext' in line and '{' in line:
                found_struct = True
            continue
        if '}' in line:
            break
        if ';' not in line:
            if '(' in line:
                # function pointer crossing more than one line
                concat_line = line
            continue
        mdef = list(filter(None, line.replace('*', ' * ').replace(';', '').replace(')', ' ) ').replace('(', ' ( ').strip().split(' ')))
        member = mdef[-1]
        if '(' in mdef:
            # it's a function pointer
            idx = mdef.index('(')
            member = mdef[idx + 2]
            AVFormatContext.append((member, 'c_void_p'))
        elif '[' in member:
            # it's an inline array
            parts = member.split('[')
            size = int(parts[1].replace(']', ''))
            member = parts[0]
            AVFormatContext.append((member, '%s * %d' % (get_ctype(mdef), size)))
        else:
            AVFormatContext.append((member, get_ctype(mdef)))
        if libavformat_version < 58 and member == 'filename':
            # The filename member has a url member after it in 58+.
            # In order to allow a compatible package to be built when the build
            # machine has 57 installed, insert the missing member.
            AVFormatContext.append(('url', 'c_char_p'))

if len(AVFormatContext) == 0:
    raise Exception('could not find AVFormatContext header')

def format_members(members, remove):
    return ''.join(map(
        lambda member: "\n        ('%s', %s)," % member,
        filter(lambda member: member[0] not in remove, AVFormatContext),
    ))

nametag_ffmpeg_headers = '''# This file is part of Nametag.
# Copyright 2020 Austin Allman and Adam Higerd
#
# Nametag is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this software; if not, see
# <https://www.gnu.org/licenses/>.

from ctypes import *

class AVDictionaryEntry(Structure):
    _fields_ = [('key', c_char_p), ('value', c_char_p)]

class AVDictionary(Structure):
    _fields_ = [('count', c_int), ('elems', POINTER(AVDictionaryEntry))]

class AVIOInterruptCB(Structure):
    _fields_ = [('callback', c_void_p), ('opaque', c_void_p)]

class AVInputFormat(Structure):
    _fields_ = [
        ('name', c_char_p),
        ('long_name', c_char_p),
        ('flags', c_int),
        ('extensions', c_char_p),
        ('codec_tag', c_void_p),
        ('priv_class', c_void_p),
        ('mime_type', c_char_p),
    ]

class AVFormatContext57(Structure):
    _fields_ = [%s
    ]

class AVFormatContext58(Structure):
    _fields_ = [%s
    ]

class AVFormatContext59(Structure):
    _fields_ = [%s
    ]
''' % (
    format_members(AVFormatContext, ['max_streams', 'url']),
    format_members(AVFormatContext, []),
    format_members(AVFormatContext, ['open_cb', 'filename']),
)

with open('nametag/ffmpeg_headers.py', 'wb') as f:
    f.write(nametag_ffmpeg_headers.encode())

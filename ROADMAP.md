Roadmap
=======

Required for v1.0
-----------------
* ~~Add --rename functionality~~
    * ~~Add basic directory scanning functionality~~
    * ~~Add ability to get tags from media~~
    * ~~Allow user to pass a pattern string and use that to format media tags into filenames safe filenames~~
    * ~~Add basic filename sanitization~~
* ~~Add argument parsing~~
    * ~~Import and use argparse~~
    * ~~Decide on a sensible general argument scheme~~
* Add preprocessing media analyzer
    * Build list of files that need to be processed and the tags associated with each file
    * Attempt to figure out what songs are part of the same album and organize such information in a sensible way
    * Attempt to figure out how many tracks and how many discs are in an album if information is missing or incorrect
* Add a class for outputting files that has built in logic on how to respond to various conflicts
    * Generate a internal list of filenames to output in advance to identify in advance when a given path will be written to multiple times
    * Identify when an output path would overwrite an already exiting file and if the file that is the same file or a different file
    * Add prompts for any conflicts that can't automatically be resolved
        * Have command-line flags to choose resolution in advance
* Disc/track numbering/renumbering
* Add --copy functionality
    * Come to a final decision on how to handle the target path (whether to dump all files into it or to automatically recreate folder structures)
    * Add way to generate folders based on metadata tags
* Add --move functionality
* Double-check sanitization and exception handling
    * Make sure tags can't generate into hidden files or reference the parent directory

Short-term features
-------------------
* Bulk tag updating
    * Figure out missing or incorrect tags based on paths and filenames
    * Make sure to have predefined settings for common media manager formats, e.g. iTunes
* Media database for speeding up incremental updates
* Portable player syncing
* xsf tagging format
* !tags.m3u / extm3u tagging support

Long-term features
------------------
* Interactive UI
    * ncurses and GUI
* Optional ID3v1 tagging for broader compatibility with old players
* Playlist management

# This file is part of Nametag.
# Copyright 2020 Austin Allman and Adam Higerd
#
# Nametag is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this software; if not, see
# <https://www.gnu.org/licenses/>.

from nametag.scan_media import scan_media
import shutil
import os

def clean_tag(value):
    INVALID_CHARACTERS = ['\\', '/', ':', '*', '"', '<', '>', '|', '?', '^', '\u00a5']
    return ''.join(
        ('_' if ch in INVALID_CHARACTERS else ch)
        for ch in value
        if ord(ch) >= 32
    ).strip()

def rename_recursive(name_format, path, dry_run = False):
    for fn, tags in scan_media(path):
        disc = str(int(tags.get('disc', '0/0').split('/')[0].strip() or '0'))
        track = str(int(tags.get('track', '0/0').split('/')[0].strip() or '0'))
        extension = os.path.splitext(fn)[1]
        if disc == '0':
            disc = ''
        else:
            disc = disc + '-'
        if track == '0':
            track = ''
        track = disc + track
        new_tags = {
            'title': tags.get('title', ''),
            'artist': tags.get('artist', ''),
            'album_artist': tags.get('album_artist', ''),
            'track': track,
            'album': tags.get('album', ''),
            'genre': tags.get('genre', ''),
            'date': tags.get('date', ''),
        }
        new_tags = { k: clean_tag(v) for (k,v) in new_tags.items() }
        new_path = os.path.join(os.path.dirname(fn), name_format.format(**new_tags).strip())
        if os.path.exists(new_path + extension):
            index = 1
            while os.path.exists(new_path + ' (' + str(index) + ')' + extension):
                index += 1
            new_path += ' (' + str(index) + ')' + extension
        else:
            new_path += extension
        print('%s\n\t-> %s' % (os.path.relpath(fn, path), os.path.relpath(new_path, path)))
        if not dry_run:
            shutil.move(fn, new_path)

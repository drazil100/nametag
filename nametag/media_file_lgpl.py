# This file is part of Nametag.
# Copyright 2020 Austin Allman and Adam Higerd
#
# Nametag is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this software; if not, see
# <https://www.gnu.org/licenses/>.

from nametag.avformat import AVInputFile
from datetime import datetime
from numbers import Number
import hashlib
import os

MD5_BLOCK_SIZE = 64 * 1024
METADATA_KEYS = ['duration', 'bitrate', 'format', 'mime_type', 'format_short_names']

class MediaFileLGPL(object):
    def __init__(self, path):
        self.path = path
        self._tags = None
        self._dirty = False
        self._checksum = None
        self._metadata = None
        # will throw if file not found
        stat = os.stat(path)
        self.filesize = stat.st_size
        self.modified_at = datetime.fromtimestamp(stat.st_mtime)

    def _get_tags(self):
        self._get_metadata()

    def _get_metadata(self):
        with AVInputFile(self.path) as song:
            if self._tags is None:
                self._tags = song.get_tags()
            metadata = {}
            for key in METADATA_KEYS:
                metadata[key] = getattr(song, key)
                if isinstance(metadata[key], Number) and metadata[key] < 0:
                    metadata[key] = None
            self._metadata = metadata

    def __getitem__(self, key):
        if self._tags is None:
            self._get_tags()
        return self._tags.get(key, None)

    def __setitem__(self, key, value):
        if self._tags is None:
            self._get_tags()
        self._tags[key] = value
        self._checksum = None
        self._dirty = True

    @property
    def md5sum(self):
        if self._checksum is None:
            md5 = hashlib.md5()
            with open(self.path, 'rb') as song:
                while True:
                    block = song.read(MD5_BLOCK_SIZE)
                    if len(block) == 0:
                        break
                    md5.update(block)
            self._checksum = md5.hexdigest()
        return self._checksum

    def get(self, key, default = None):
        value = self[key]
        if value is None:
            return default
        return value

    @property
    def duration_ms(self):
        if self._metadata is None:
            self._get_metadata()
        seconds = self._metadata.get('duration', None)
        if seconds is None:
            return None
        return seconds * 1000.0

def make_getter(key):
    @property
    def getter(self):
        if self._metadata is None:
            self._get_metadata()
        return self._metadata.get(key, None)
    return getter
for key in METADATA_KEYS:
    setattr(MediaFileLGPL, key, make_getter(key))

# This file is part of Nametag.
# Copyright 2020 Austin Allman and Adam Higerd
#
# Nametag is free software; you can redistribute this file or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 3 of
# the License, or (at your option) any later version.
#
# If this file, media_file.py, is omitted, you can redistribute and/or
# modify Nametag under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation, either version
# 2.1 of the License, or (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# and the GNU Lesser General Public License along with this software.
# If not, see <https://www.gnu.org/licenses/>.


# Use a private installation of taglib to avoid stomping on anything in site-packages and to
# avoid depending on Debian's painfully out-of-date python3-taglib package.
from nametag import taglib
from nametag.media_file_lgpl import MediaFileLGPL

TAG_MAPPING = {
    'ALBUM': 'album',
    'ALBUMARTIST': 'album_artist',
    'ARTIST': 'artist',
    'DISCNUMBER': 'disc',
    'TRACKNUMBER': 'track',
    'TITLE': 'title',
    'GENRE': 'genre',
    'DATE': 'date',
}

class MediaFile(MediaFileLGPL):
    def __init__(self, path):
        super().__init__(path)
        self._duration = None
        self._bitrate = None

    def _get_tags(self):
        song = taglib.File(self.path)
        self._tags = { TAG_MAPPING.get(tag, tag): song.tags[tag][0] for tag in song.tags }
        self._duration = song.length
        self._bitrate = song.bitrate

    @property
    def duration(self):
        if self._duration is None:
            self._get_tags()
        return self._duration

    @property
    def bitrate(self):
        if self._bitrate is None:
            self._get_tags()
        return self._bitrate

    @property
    def duration_ms(self):
        ms = super().duration_ms
        if ms is None:
            return self.duration * 1000.0
        return ms


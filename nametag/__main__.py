# This file is part of Nametag.
# Copyright 2020 Austin Allman and Adam Higerd
#
# Nametag is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this software; if not, see
# <https://www.gnu.org/licenses/>.

from nametag.nametag import rename_recursive
import sys
import argparse

parser = argparse.ArgumentParser(description="Rename music files based on their id3 metadata tags.")
parser.add_argument("path", nargs='+', help="An input file or directory must be specified")

output_mode = parser.add_mutually_exclusive_group()
output_mode.add_argument("-c", "--copy", metavar="TARGET", help="Copy files to TARGET without removing the old files.")
output_mode.add_argument("-m", "--move", metavar="TARGET", help="Move files to TARGET and delete old files. (Will only delete if move is successful)")

parser.add_argument("-r", "--rename", metavar="PATTERN", help="Used to specify how you wish your files to be named.")
parser.add_argument("-p", "--pretend", action="store_true", help="Performs a dry run without modifying the filesystem or any files.")

args = parser.parse_args()

print(args)

def rename_mode(args):
    for p in args.path:
        print(p)
        rename_recursive(args.rename, p, args.pretend)

def copy_mode(args):
    print("Copy has not been implemented yet.")

def move_mode(args):
    print("Move has not been implemented yet.")


if args.copy is None and args.move is None:
    if args.rename is not None:
        rename_mode(args)
    else:
        print("Must specify -r/--rename.")

if args.copy is not None:
    copy_mode(args)

if args.move is not None:
    move_mode(args)

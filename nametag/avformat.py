# This file is part of Nametag.
# Copyright 2020 Austin Allman and Adam Higerd
#
# Nametag is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this software; if not, see
# <https://www.gnu.org/licenses/>.

import ctypes, warnings
from nametag.ffmpeg_headers import *

# Pick the right DLL format
try:
    dll = ctypes.windll
    extension = 'dll'
except AttributeError:
    dll = ctypes.cdll
    extension = 'so'

libavformat = dll.LoadLibrary('libavformat.%s' % extension)
libavutil = dll.LoadLibrary('libavutil.%s' % extension)

# Optional in newer versions of ffmpeg
libavformat.av_register_all()

avformat_version = libavformat.avformat_version() >> 16
if avformat_version == 57:
    AVFormatContext = AVFormatContext57
elif avformat_version == 58:
    AVFormatContext = AVFormatContext58
elif avformat_version == 59:
    AVFormatContext = AVFormatContext59
elif avformat_version < 57:
    warnings.warn('Unsupported libavformat version %s detected, using version 57.' % avformat_version)
    AVFormatContext = AVFormatContext57
elif avformat_version > 59:
    warnings.warn('Unsupported libavformat version %s detected, using version 59.' % avformat_version)
    AVFormatContext = AVFormatContext59

class AVError(Exception):
    def __init__(self, errno):
        self.errno = errno
        buf = ctypes.create_string_buffer(100)
        err = libavutil.av_strerror(errno, buf, 100)
        if err != 0:
            raise Exception('error decoding error %d' % errno)
        super().__init__(buf.value.decode())

class AVInputFile(object):
    def __init__(self, path):
        self._tags = None
        self._fmt = None
        fmt = ctypes.POINTER(AVFormatContext)()
        p_fmt = ctypes.pointer(fmt)
        err = libavformat.avformat_open_input(p_fmt, path.encode(), None, None)
        if err != 0:
            self._fmt = None
            raise AVError(err)
        self._p_p_fmt = p_fmt
        self._p_fmt = fmt
        self._fmt = fmt.contents

        # Pull out useful format fields
        self.duration = self._fmt.duration / 1000000.0
        self.bitrate = self._fmt.bit_rate
        iformat = ctypes.cast(self._fmt.iformat, ctypes.POINTER(AVInputFormat)).contents
        self.format_short_names = iformat.name.decode().split(',')
        self.format = iformat.long_name
        self.mime_type = iformat.mime_type

    def __del__(self):
        self.close()

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.close()
        return None

    def close(self):
        if self._fmt:
            libavformat.avformat_free_context(self._p_fmt)
            self._fmt = None
        self._p_fmt = None
        self._p_p_fmt = None

    def get_tags(self):
        tags = {}
        metadata = ctypes.cast(self._fmt.metadata, ctypes.POINTER(AVDictionary)).contents
        elements = ctypes.cast(metadata.elems, ctypes.POINTER(AVDictionaryEntry * metadata.count)).contents
        for element in elements:
            tags[element.key.decode()] = element.value.decode()
        self._tags = tags
        return self._tags

    def __getitem__(self, key):
        if self._tags is None:
            self.get_tags()
        return self._tags.get(key, None)

    def __setitem__(self, key, value):
        if self._tags is None:
            self.get_tags()
        self._tags[key] = str(value)

    def __delitem__(self, key):
        if self._tags is None:
            self.get_tags()
        del self._tags[key]

    def metadata_items(self):
        if self._tags is None:
            self.get_tags()
        return self._tags.items()

# This file is part of Nametag.
# Copyright 2020 Austin Allman and Adam Higerd
#
# Nametag is free software; you can redistribute it and/or
# modify it under the terms of the GNU Lesser General Public
# License as published by the Free Software Foundation; either
# version 2.1 of the License, or (at your option) any later version.
#
# This software is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with this software; if not, see
# <https://www.gnu.org/licenses/>.

import subprocess
import os
import re
from nametag.media_file import MediaFile

def scan_media(root):
    """
    Scans a specified path for media files.

    Returns a generator yielding (filename, tags) for each file found.
    """
    if not os.path.isdir(root):
        yield (root, get_tags(root))
        return
    for root, dirs, files in os.walk(root):
        for fn in files:
            try:
                path = os.path.join(root, fn)
                song = MediaFile(path)
                # TODO: We have to eagerly invoke the internal method in order
                # to consume errors here before the caller can see them. We
                # should only consume errors that prevent the file from being
                # read at all, and let the caller deal with errors trying to
                # parse the contents instead of calling an internal method.
                song._get_tags()
                yield (path, song)
            except Exception as e:
                print(e)
                pass

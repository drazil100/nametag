HERE = $(dir $(realpath $(firstword $(MAKEFILE_LIST))))

help:
	@echo "Available targets:"
	@echo "  enter        Activates a virtualenv for development in a subshell"
	@echo "  dep          Updates or installs dependencies"
	@echo "  clean        Removes virtualenv, dependencies, and compiled files"
	@echo "  install      Globally installs nametag and its private dependencies"
	@echo "  uninstall    Removes a global installation of nametag"

bin/activate:
	python3 -m venv .

enter: bin/activate
	$$SHELL --rcfile bin/activate -i

lib/pip3.installed: nametag/ffmpeg_headers.py bin/activate requirements.txt
	/bin/bash -c "source bin/activate && pip3 install --upgrade -r requirements.txt -t nametag"
	touch lib/pip3.installed

nametag/taglib*.so: lib/pip3.installed

lib/taglib.installed: lib/pip3.installed nametag/taglib*.so
	touch lib/taglib.installed

nametag/ffmpeg_headers.py: parse_ffmpeg_headers.py
	python3 parse_ffmpeg_headers.py

bin/nametag: cli.py
	chmod u+x cli.py
	ln -s $(HERE)/cli.py bin/nametag

dep: nametag/ffmpeg_headers.py bin/activate lib/taglib.installed bin/nametag
	rm -f nametag/pyprinttags.py

clean:
	git clean -dXf

install: dep
	mkdir -p /usr/lib/nametag/nametag
	install cli.py /usr/lib/nametag/
	cp nametag/*.py nametag/*.so /usr/lib/nametag/nametag/
	find /usr/lib/nametag/ -name "*.py" | xargs python3 -m py_compile 
	ln -sf /usr/lib/nametag/cli.py /usr/bin/nametag

uninstall:
	rm -rf /usr/lib/nametag
	rm /usr/bin/nametag
